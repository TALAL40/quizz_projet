<?php
session_start();

if (isset($_POST['input'])) {
  if ($_POST['input'] == "ligue2") {
    header('Location: palier2.php');
    exit;

  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
  <title>Document</title>
</head>

<body class="p2">
<!-- premier palier -->
<div class="container_question">
    <h2>Palier 1</h2>
    <form method="POST" action="palier1.php">

        <div class="question" id="question1">
            <h3>Quand a eu lieu la première Coupe du monde ?</h3>
            <input type="radio" name="question1" value="A" required onclick="paysQ()"> Argentine<br><br>
            <input type="radio" name="question1" value="B" required onclick="paysQ()"> Brésil <br><br>
            <input type="radio" name="question1" value="C" required onclick="paysQ()"> Uruguay <br><br>
            <button type="button" onclick="validateAnswer('monde')">Valider la réponse</button>
        </div>
        <div class="question" id="question2">
            <h3>Quel est le surnom donné au Gambie ?</h3>
            <input type="radio" name="question2" value="D" required onclick="gambieQ()"> Les lions<br><br>
            <input type="radio" name="question2" value="E" required onclick="gambieQ()">Les scorpions<br><br>
            <input type="radio" name="question2" value="F" required onclick="gambieQ()">Les Aigles vertes<br><br>
            <button type="button" onclick="validateAnswer('gambie')">Valider la réponse</button>
        </div>
        <div class="question" id="question3">
            <h3> Qui a gagné le ballon d'or en 2019 ?</h3>
            <input type="radio" name="question3" value="" required onclick="ballonQ()"> Messi<br><br>
            <input type="radio" name="question3" value="H" required onclick="ballonQ()"> Ronaldo<br><br>
            <input type="radio" name="question3" value="J" required onclick="ballonQ()"> Modric<br><br>
            <button type="button" onclick="validateAnswer('ballon')">Valider la réponse</button>
        </div>
    </form>

</div>
<input class="start" type="submit" name="input" value="ligue2" id="ligua" >

<!-- Local javasvript file -->
<script src="script.js"></script>
</body>

</html>