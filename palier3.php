
<?php
session_start();

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
  <title>Document</title>
</head>

<body class="4">

<div class="container3">
  <h2>basketball &  volley-ball & Rugby</h2>
  
  <form method="POST" action="réponse.php">
  
        <div class="question" id="basket1">
          <h3> Quelle équipe de rugby effectue une célèbre danse avant chaque match ?</h3>
          <input type="radio" name="question1" value="A"> Australie<br>
          <input type="radio" name="question1" value="B"> Afrique du Sud <br>
          <input type="radio" name="question1" value="C"> Nouvelles Zélande <br>
          <button type="button" onclick="validateAnswer('nba')">Valider la réponse</button>
        </div>
        <div class="question" id="basket2">
          <h3>Quel est le sport le plus pratiqué dans le monde ?</h3>
          <input type="radio" name="question2" value="D"> Football<br>
          <input type="radio" name="question2" value="E"> Natation<br>
          <input type="radio" name="question2" value="F"> Basketball<br>
          <button type="button" onclick="validateAnswer('volly')">Valider la réponse</button>
        </div>
        <div class="question" id="basket3">
          <h3> Combien de Grands Chelems a Roger Federer ?</h3>
          <input type="radio" name="question3" value="G"> 20<br>
          <input type="radio" name="question3" value="H"> 22<br>
          <input type="radio" name="question3" value="J"> 24<br>
          <button type="button" onclick="validateAnswer('ballon')">Valider la réponse</button>
        </div>
      </form>
</div>
    
 
  

  <script src="script.js"></script>
  </body>

</html>