<?php
session_start();

if (isset($_POST['input'])) {
  if ($_POST['input'] == "ligue1") {
    header('Location: palier3.php');
    exit;

  }
}
?>


?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
  <title>Document</title>
</head>

<body class="p3">
  <div class="container2"> 
  <h2>basketball & volley-ball & Rugby</h2>

    <form method="POST" action="palier3.php">

      <div class="question" id="basket1">
        <h3> Qui a gagné le NBA en 2021?</h3>
        <input type="radio" name="question1" value="A"> LOS ANGELES LAKERS<br>
        <input type="radio" name="question1" value="B"> BOSTON CELTICS <br>
        <input type="radio" name="question1" value="C"> GOLDEN STATE <br>
        <button type="button" onclick="validateAnswer('nba')">Valider la réponse</button>
      </div>
      <div class="question" id="basket2">
        <h3>Dans quel pays le volley-ball a-t-il été inventé ?</h3>
        <input type="radio" name="question2" value="D"> USA<br>
        <input type="radio" name="question2" value="E">Pays bas<br>
        <input type="radio" name="question2" value="F">Espagne<br>
        <button type="button" onclick="validateAnswer('volly')">Valider la réponse</button>
      </div>
      <div class="question" id="basket3">
        <h3> Combien de joueurs composent une équipe de rugby ?</h3>
        <input type="radio" name="question3" value="G"> 14<br>
        <input type="radio" name="question3" value="H"> 15<br>
        <input type="radio" name="question3" value="J"> 13<br>
        <button type="button" onclick="validateAnswer('ballon')">Valider la réponse</button>
      </div>
    </form>

  </div>
  <input class="start" type="submit" name="input" value="Niveau2">

  <script src="script.js"></script>
</body>

</html>